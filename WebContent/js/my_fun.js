var map;                        //map对象
var content;                    //信息窗口字符串
var index;                      //长度/数量索引
var result;                     //中间结果
var addorupdate = 0;             //添加或者更新
var itemid;                     //存储id
function useAjax(method,url,dataType,jsondata)
{
    $.ajax({
        type : method,
        url : url,
        dataType :dataType,
        data :jsondata ,
        success : function(opt)
        {
            result = opt;
        },
        error : function(xhr, status, errMsg) {
            alert("");
        }
    });
    return result;
}

$(function()
{

    var auto_str;
    //自动完成
    $("#keyword").autocomplete({
        source: function (request, response) {
            auto_str = "[";
            $.ajax({
                type : "POST",
                url : "search",
                dataType :"text",
                data :{
                    "keyValue" :  $("#keyword").val()
                } ,
                success : function(opt)
                {
                    //result = opt;
                    $.each(eval(opt), function (index, item) {
                        auto_str +=  '"'+item.poi_title+'",';
                    });
                    auto_str = auto_str.slice(0,-1);
                    auto_str += "]";
                    response(eval(auto_str));
                },
                error : function(xhr, status, errMsg) {
                    alert("");
                }
            });
            //$.each(eval(useAjax("POST", "search","text" ,{"keyValue" :  $("#keyword").val()})), function (index, item) {
            //    auto_str +=  '"'+item.poi_title+'",';
            //});
        }
    });

    //回车搜索
    $("#keyword").keyup(function(event) {
        if (event.keyCode == 13) {
            begin_search();
        }
    });
});

$(function()
{
    var result;
    var auto_str;
    // 百度地图API功能
    map = new BMap.Map('map');
    var firstpoi = new BMap.Point(114.443978,30.566316);
    map.centerAndZoom(firstpoi, 11);
    map.enableScrollWheelZoom();

    var local = new BMap.LocalSearch(map, {
        renderOptions:{map: map,panel:"detail"}
    });
    $("#search").click(function()
    {
        begin_search();
    });
});

function panelOver(n)
{
    $("#panel"+n).stop().animate({"background-color":"#95caca"},100);
    $("#delete"+n).css({"visibility":"visible"});
    $("#update"+n).css({"visibility":"visible"});
}
function panelOut(n)
{
    $("#panel"+n).stop().animate({"background-color":"#ffffff"},100);
    $("#delete"+n).css({"visibility":"hidden"});
    $("#update"+n).css({"visibility":"hidden"});
}

function deleteItem(n)
{
    $.ajax({
       type : "GET",
       url : "deleteUpdate",
       dataType : "text",
       data : {
           "poi_id" :  $("#poi_id"+n).text()
       },
       success : function(result)
       {
            alert(result);
            begin_search();
       },
       error : function(xhr, status, errMsg)
       {
           alert("删除失败!");
       }
    });
}

function updateItem(n)
{
    addorupdate = 0;
    itemid = $("#poi_id"+n).text();
    $("#poi_title").val($("#poi_title"+n).text());
    $("#poi_address").val($("#poi_address"+n).text().split(":")[1]);
    $("#poi_tags").val($("#poi_tags"+n).text().split(":")[1]);
    $("#poi_phone").val($("#poi_phone"+n).text().split(":")[1]);
    $("#poi_lng").val($("#poi_lng"+n).text().split(":")[1]);
    $("#poi_lat").val($("#poi_lat"+n).text().split(":")[1]);
    $("#modify").window("open");
}

function  packageItem()
{
    return  '{"poi_title":"'+$("#poi_title").val()+'",'+
            '"poi_address":"'+$("#poi_address").val()+'",'+
            '"poi_tags":"'+$("#poi_tags").val()+'",'+
            '"poi_photel":"'+$("#poi_phone").val()+'",'+
            '"poi_lng":"'+$("#poi_lng").val()+'",'+
            '"poi_lat":"'+$("#poi_lat").val()+'"}';
}
function addItem()
{
    addorupdate = 1;
    $("#poi_title").val("");
    $("#poi_address").val("");
    $("#poi_tags").val("");
    $("#poi_phone").val("");
    $("#poi_lng").val("");
    $("#poi_lat").val("");
    $("#modify").window("open");

}
function add_update()
{
    var url = addorupdate === 0 ? "deleteUpdate" : "addOrAddbulk";
    $.ajax({
       type : "POST",
       url : url,
       dataType : "text",
       data : {
           "id": itemid,
           "item" :  packageItem()
       },
       success : function(result)
       {
            alert(result);
            begin_search();
       },
       error : function(xhr, status, errMsg)
       {
           alert("添加更新失败!");
       }
    });
    $("#modify").window("close");

}
//少用
function getLength(index)
{
    var i = 0;
    for(var item in index)
    {
        i++;
    }
    return i;
}

function addMarker(value,n)
{
    content = '<div style="margin:0;line-height:20px;padding:2px;">' +
        '<img src="image/img01.jpg" alt="" style="float:right;zoom:1;overflow:hidden;width:100px;height:100px;margin-left:3px;"/>' +
        value.poi_address.toString()+'<br/>'+value.poi_photel+'<br/></div>';

    //创建检索信息窗口对象
    var searchInfoWindow = new BMapLib.SearchInfoWindow(map, content, {
        title  : value.poi_title,      //标题
        width  : 290,             //宽度
        height : 105,              //高度
        panel  : "panel",         //检索结果面板
        enableAutoPan : true,     //自动平移
        searchTypes   :[
            BMAPLIB_TAB_SEARCH,   //周边检索
            BMAPLIB_TAB_TO_HERE,  //到这里去
            BMAPLIB_TAB_FROM_HERE //从这里出发
        ]
    });
    var marker = new BMap.Marker(new BMap.Point(value.poi_lng, value.poi_lat));
    marker.addEventListener("click", function(e){
        searchInfoWindow.open(marker);
    });
    map.addOverlay(marker); //在地图中添加marker
    if(n === 9)
    {
        var new_point = new BMap.Point(value.poi_lng, value.poi_lat);
        map.panTo(new_point);
    }
    content = "";
}
function searchResultToshow(jsonArrayPoi)
{
    var show_str; //展现字符串
    map.clearOverlays();
    $("#show").empty();
    $("#count").html(getLength(eval(jsonArrayPoi)));
    $.each(eval(jsonArrayPoi), function (n, value)
    {
        show_str = '<div onmouseover="panelOver('+n+');" onmouseout="panelOut('+n+');" id="panel'+n+'" class="mt_10 ml_30 f_l line_h20">'+
            '<table class="f_l"><tr>'+
            '<td><div id = "poi_title'+n+'" class="fs_1_4 c_blue fw_b mouse-pointer" onclick="show_detail('+n+');">'+value.poi_title+'</div>'+
            '</td></tr><tr><td><div id = "poi_address'+n+'" class="f_l">地址:'+value.poi_address+'</span></div>'+
            '</td></tr><tr><td>'+
            '<div id = "poi_phone'+n+'" class="f_l w_125">电话:'+value.poi_photel+'</div>'+
            '<div id = "poi_tags'+n+'" class="f_l ml_80 w_125">类别:'+value.poi_tags+'</div>'+
            '</td></tr><tr><td><div id = "poi_lng'+n+'" class="f_l w_125">经度:'+value.poi_lng+'</div>'+
            '<div class="f_l ml_80 w_125" id = "poi_lat'+n+'">纬度:'+value.poi_lat+'</div>'+
            '</td></tr></table><table class="operate f_l w_125"><tr><td>'+
            '<div id="delete'+n+'" onclick="deleteItem('+n+');" style="text-align: center;float: right;margin-right: 10px;visibility: hidden"><a href="#" class="mt_10 ml_8">删除</a></div>'+
            '<div id="update'+n+'" onclick="updateItem('+n+');" style="text-align: center;float: right;visibility: hidden"><a href="#" class="mr_8 mt_10">修改</a></div>'+
            '</td></tr></table></div><div class="vis_hide" id="poi_id'+n+'">'+value.id+'</div>';
        $("#show").append(show_str);
        addMarker(value,n);
    });
}

function show_detail (n)
{
    var id_str = $("#poi_id"+n).text();
    $("#poi_title_show").text($("#poi_title"+n).text());
    $("#poi_address_show").text($("#poi_address"+n).text().split(":")[1]);
    $("#poi_tags_show").text($("#poi_tags"+n).text().split(":")[1]);
    $("#poi_phone_show").text($("#poi_phone"+n).text().split(":")[1]);
    $("#poi_lng_show").text($("#poi_lng"+n).text().split(":")[1]);
    $("#poi_lat_show").text($("#poi_lat"+n).text().split(":")[1]);
    $("#showDetail").window("open");
}

function begin_search()
{
    //var show_str;
    $.ajax({
        type : "GET",
        url : "search",
        dataType :"text",
        data :{
            "keyValue" :  $("#keyword").val()
        } ,
        success : function(opt)
        {
            searchResultToshow(opt);
            //result = opt;
        },
        error : function(xhr, status, errMsg) {
            alert("主题函数出问题，检索失败！");
        }
    });
   // searchResultToshow(useAjax("GET", "search", "text", {"keyValue" :  $("#keyword").val()}));
}