/**
 * Created by Forever Road on 2017/4/26.
 */
function Test(options)
{
    this.options = options || {};
}

Test.prototype.constructor = Test;

Test.prototype.getName = function()
{
    return this.name;
};

Test.prototype.getAge = function()
{
    return this.age;
};

Test.prototype.setName = function(name)
{
    this.name = name;
};

Test.prototype.setAge = function(age)
{
    this.age = age;
};

var test = new Test();
test.setName("张发勇");
alert(test.getName());

function Test1()
{
    cx.base(this, null);
}


Test1.setName = function(name)
{
    alert(name);
    cx.base(this, "setName");
};

var test1 = new Test1();
test1.setName("张发勇1");
alert(test1.getName());