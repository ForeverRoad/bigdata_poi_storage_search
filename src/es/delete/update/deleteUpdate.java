package es.delete.update;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import esInterface.operation;
import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class deleteUpdate
 */
@WebServlet("/deleteUpdate")
public class deleteUpdate extends HttpServlet {
	//接口操作对象
	operation myQuery = new operation();
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public deleteUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 *	删除操作
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		
		String poi_id = request.getParameter("poi_id");
		
		////////////////////////////////////////////
		
		boolean result = myQuery.delete(poi_id);
		if (result)
		{
			 PrintWriter out = response.getWriter();
			 out.print("delete success!");	
		}
		///////////////////////////////////////////
	}

	/**
	 * 修改操作
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");

		String id = request.getParameter("id");
		String item = request.getParameter("item");
		////////////////////////////////////////////
		
		boolean result = myQuery.update(item, id);
		if (result)
		{
			 PrintWriter out = response.getWriter();
			 out.print("update success!");	
		}
		///////////////////////////////////////////
	}

}
