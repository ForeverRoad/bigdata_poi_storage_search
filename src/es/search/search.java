package es.search;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ObjectUtils.Null;

import esInterface.operation;
import net.sf.json.JSONArray;
/**
 * Servlet implementation class search
 */
@WebServlet("/search")
public class search extends HttpServlet {
	
	//接口操作对象
	operation myQuery = new operation();
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public search() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		
		String keyword = request.getParameter("keyValue");
		
		////////////////////////////////////////////
		
		JSONArray result = myQuery.Query(keyword);
		
        PrintWriter out = response.getWriter();
        out.print(result);
		///////////////////////////////////////////
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		
		String keyword = request.getParameter("keyValue");
		
		////////////////////////////////////////////
		
		JSONArray result = myQuery.poi_title_Query(keyword);
		
        PrintWriter out = response.getWriter();
        out.print(result);
		///////////////////////////////////////////
	}

}
