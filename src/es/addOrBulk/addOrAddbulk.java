package es.addOrBulk;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import esInterface.operation;

/**
 * Servlet implementation class addOrAddbulk
 */
@WebServlet("/addOrAddbulk")
public class addOrAddbulk extends HttpServlet {
	//接口操作对象
	operation myQuery = new operation();
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public addOrAddbulk() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * 添加
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		
		String addItem = request.getParameter("item");
		
		////////////////////////////////////////////
		
		boolean result = myQuery.add(addItem);
		if (result)
		{
			 PrintWriter out = response.getWriter();
			 out.print("add success!");	
		}
		///////////////////////////////////////////	}
	}
}
