package esInterface;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import com.fasterxml.jackson.databind.deser.DataFormatReaders.Match;

public class operation
{
	//客户端设置
	Settings settings = null;
	//创建客户端
	public static TransportClient client = null;
	
	//构造函数初始化
	public operation()
	{
		super();
		settings = Settings.builder()
					.put("cluster.name", "cxy")
						.put("client.transport.sniff", "true")
							.build();
		try
		{
			client = new PreBuiltTransportClient(settings)
						.addTransportAddress(new InetSocketTransportAddress
							(InetAddress.getByName("127.0.0.1"), 9300));
		} 
		catch (UnknownHostException e)
		{
			e.printStackTrace();
		}
	}
	
	//检索主体函数
	public JSONArray Query(String keyword)
	{
		//单字段查询
		MatchQueryBuilder query = QueryBuilders.matchQuery("poi_title", "xxxx大学");
		//多字段查询字段
		MultiMatchQueryBuilder query = 
				QueryBuilders.multiMatchQuery(keyword, "poi_address","poi_title","poi_tags");
		//高亮
		HighlightBuilder highlight = new HighlightBuilder();
		highlight.preTags("<span class = 'highlight'>")
				.postTags("</span>")
					.field("poi_title")
						.field("poi_address")
							.field("poi_tags");
		//检索设置
		SearchResponse response = client.prepareSearch("pois")
				.setTypes("cxyword")
					.setQuery(query)
						.highlighter(highlight)
							.setFrom(0)
								.setSize(5)
									.execute()
										.actionGet();
		//执行检索
		SearchHits hits = response.getHits();
		//以json数组存储搜索结果
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		System.out.println("共搜到："+hits.getTotalHits()+"条结果");
		for (SearchHit hit : hits)
		{
			jsonObject = new JSONObject();
			
			//首先判断该字段是否存在高亮，存在则拉取高亮，不存在则拉取整个字段
			jsonObject.put("poi_title", 
					hit.getHighlightFields().get("poi_title") != null ?
							getFraStr(hit.getHighlightFields().get("poi_title").getFragments()):
							hit.getSource().get("poi_title"));
			
			jsonObject.put("poi_address", 
					hit.getHighlightFields().get("poi_address") != null ?
							getFraStr(hit.getHighlightFields().get("poi_address").getFragments()):
							hit.getSource().get("poi_address"));

			jsonObject.put("poi_tags", 
					hit.getHighlightFields().get("poi_tags") != null ?
							getFraStr(hit.getHighlightFields().get("poi_tags").getFragments()):
							hit.getSource().get("poi_tags"));
			
			jsonObject.put("poi_lat", hit.getSource().get("poi_lat"));
			jsonObject.put("poi_lng", hit.getSource().get("poi_lng"));
			jsonObject.put("poi_photel", hit.getSource().get("poi_photel"));
			jsonObject.put("id", hit.getId());
			jsonArray.add(jsonObject);
		}
		return jsonArray;
	}
	
	//自动完成
	public JSONArray poi_title_Query(String keyword)
	{
		
		MultiMatchQueryBuilder query = 
				QueryBuilders.multiMatchQuery(keyword, "poi_title");
		
		//highlight
		HighlightBuilder highlight = new HighlightBuilder();
		
//		highlight.preTags("")
//					.field("poi_title");
		SearchResponse response = client.prepareSearch("pois")
				.setTypes("cxyword")
					.setQuery(query)
						.highlighter(highlight) //highlight
							.setFrom(0)
								.setSize(5)
									.execute()
										.actionGet();

		SearchHits hits = response.getHits();
		//存储搜索结果
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		System.out.println("推荐："+hits.getTotalHits()+"条结果");
		
		for (SearchHit hit : hits)
		{
			jsonObject = new JSONObject();
			
			jsonObject.put("poi_title",hit.getSource().get("poi_title"));

			jsonObject.put("id", hit.getId());
			jsonArray.add(jsonObject);
		}
		return jsonArray;
	}
	//修改主体函数
	public boolean update(String jsonItem, String id)
	{
		JSONObject jsonObject = JSONObject.fromObject(jsonItem);
		
		UpdateRequest updateRequest = new UpdateRequest();
		updateRequest.index("pois").type("cxyword").id(id);
		updateRequest.doc(jsonObject);
		try
		{
			client.update(updateRequest).get();
		} 
		catch (InterruptedException | ExecutionException e)
		{
			e.printStackTrace();
		}
		return true;
	}
	
	//删除主体函数
	public boolean delete(String id)
	{
		client.prepareDelete("pois", "cxyword", id).get();
		return true;
	}
	//添加主体函数
	public boolean add(String jsonItem)
	{
		JSONObject item = JSONObject.fromObject(jsonItem);
		client.prepareIndex("pois", "cxyword").setSource(item).get();
		return true;
	}
	//字符串拼接
	public String getFraStr(Text text[])
	{
		for (Text str:text)
		{
			return str.string();
		}
		return null;
	}
}
